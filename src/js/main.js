document.body.addEventListener('click', function(e) {
    const hambMain = document.querySelector('.hamburger__menu');
    const hambCheck = document.querySelector('.hamburger__toggle');
    const mask = document.querySelector('.mask');

    let target = e.target;

    if (target.className === 'hamburger__btn-lines' || el.target.className === 'hamburger__btn') {
        mask.classList.add('show');
    }

    if (target === mask) {
        mask.classList.remove('show');
        hambMain.classList.remove('show');
        hambMain.classList.add('hide');
        hambCheck.checked = false;
    } else {
        hambMain.classList.remove('hide');
        hambMain.classList.add('show');
    }
});