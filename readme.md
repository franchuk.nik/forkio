<h1>Forkio</h1>

Step project Forkio

**List of technologies used:**

1. HTML5
2. CSS3
3. SCSS
4. BEM
5. JS
6. Node.js
7. npm
8. Git


**Command staff:**
1. Liashenko Sergii 
2. Franchuk Nikolay


3. tasks: 
- Liashenko Sergii: gulp settings, html file, header.scss, editor.scss, main.js(burger menu logic)
- Franchuk Nikolay: init structure, advantages.scss, about-fork.scss, pricing.scss


gitHub pages: https://hugeoctopus.github.io/forkio 


Есть изветсная проблема с browser-sync пакетом: https://github.com/BrowserSync/browser-sync/issues/1015. 
Суть проблемы: сразу после сборки gulp-ом медленно или совсем не загружаются некоторые ресурсы (например картинки). В консоли в devTool может быть одна или несколько ошибок: Failed to load resource: the server responded with a status of 404 (Not Found). 

После одной (иногда нескольких) перезагрузки страници в браузере все проходид
